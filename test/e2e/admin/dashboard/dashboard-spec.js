// This can be one test. I was just testing something.

describe('Fill in email login form', function() {
    browser.get('#/auth/login');

    it("should load", function () {
        var e = element.all(By.model('user.email'));
        e.sendKeys('jared@email.com');
        expect(browser.getTitle()).toBe('PostLaMode');
    });
});

describe('Fill in password login form', function() {
    browser.get('#/auth/login');

    it("should load", function () {
        var e = element.all(By.model('user.password'));
        e.sendKeys('test');
        expect(browser.getTitle()).toBe('PostLaMode');
    });
});
