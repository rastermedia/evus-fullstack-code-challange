describe('todomvc', function() {
    browser.get('/');

    it("should load", function () {
        expect(browser.getTitle()).toBe('PostLaMode');
    });
});


describe('Fill in forgot password form', function() {
    browser.get('#/auth/forgot');

    it("should load", function () {
        var e = element.all(By.model('user.email'));

        e.sendKeys('jared@email.com');

        element(By.id("submitForm")).click().then(function() {

            expect(browser.getLocationAbsUrl()).toBe("/admin/dashboard");
        });
    });
});
