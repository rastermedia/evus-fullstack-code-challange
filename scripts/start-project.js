'use strict';

var systemsConfig = require('config');
var cDir = __dirname + '/../config/';
var env = require('node-env-file');
var grunt = require('grunt');

env(__dirname + '/../.env');

var _getConfigs = function() {
    if (process.env && process.env.hasOwnProperty('ENV')) {
        var cEnv = process.env.ENV;
        var cConfig = { environment: 'development' };

        try {
            cConfig = require(cDir + cEnv + '.json');
            cConfig.environment = cEnv;
            return cConfig;
        } catch (e) {
            console.error(e.toString());
            return cConfig;
        }

    } else if (process.env && process.env.hasOwnProperty('NODE_ENV')) {
        systemsConfig.environment = process.env.NODE_ENV;
        return systemsConfig;
    } else {
        console.error('Error: Failed to provide env settings!');
        return { environment: 'development' };
    }
}

//block code
{
    global.configurations = _getConfigs();
    var arg = process.argv[2] || null;

    var showMessage = function(set) {
        console.log('Building for ' + set + ' please wait...' + '\n');
    }

    if (arg && arg === 'compile') {
        grunt.tasks("build");
        return;
    }

    if (configurations.environment === 'production') {
        showMessage(configurations.environment);
        grunt.tasks("serve:dist");
    } else if (configurations.environment === 'development') {
        showMessage(configurations.environment);
        grunt.tasks("serve");
    } else if (configurations.environment === 'staging') {
        showMessage(configurations.environment);
        grunt.tasks("serve");
    } else {
        console.error('Error: Failed to get environment running in development');
        showMessage(development);
        grunt.tasks("serve");
    }
}
