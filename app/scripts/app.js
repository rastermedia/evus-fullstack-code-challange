'use strict';

/**
 * Directories for $states. Each time you create a new layout page,
 * the views should be in a separate folder, like Admin is.
*/
var viewDir = 'views/';
var appDir = viewDir + 'app/';
var authDir = viewDir + 'auth/';
var adminDir = viewDir + 'admin/';
var errorDir = viewDir + 'error/';

/**
 * This is for the loopback URL configuration
*/
var env;
var googleTrackingId;

/**
 * Logic for the LoopBack API to get the correct URI based on the env.
*/
var apiConfig = {
    api: {
        url: (function () {
            // Development
            if (~['http://0.0.0.0:8080', 'http://127.0.0.1:8080', 'http://localhost:8080'].indexOf(window.location.origin)) {
                env = 'development';
                googleTrackingId = 'UA-XXXXXX-X';
                return 'http://127.0.0.1:3000/api/';
            }

            // Staging
            if (~['STAGING-URL'].indexOf(window.location.origin)) {
                env = 'staging';
                googleTrackingId = 'UA-XXXXXX-X';
                return 'STAGING-API-URL';
            }

            // Production
            if (~['PRODUCTION-URL'].indexOf(window.location.origin)) {
                env = 'production';
                googleTrackingId = 'UA-XXXXXX-X';
                return 'PRODUCTION-API-URL';
            }

            /**
             * No defaults as it would cause confusion as to what database/api you are connected to
             * Your environment should be listed before this
            */
            console.error("If the app is not loading, a possible issue is that the URL for the page is not matching the config in app.js", window.location.origin);

            return null;
        })()
    }
};

/**
 * Inject all the needed packages and define the 'app' variable module
 *
 * @TODO We need to go through these ingections and only include
 * the ones we need. I want all the other packages to be pasted
 * at the bottom of this file in a comment with their respective
 * Github Repo url above each package.
 * We can then lazy load the packages in each controller as needed.
*/
var app = angular.module('EvusBoilerplate', [
    'config',
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    // 'ngTouch',
    'ngStorage',
    'lbServices',
    'ngMessages',
    'picardy.fontawesome',
    'ui.bootstrap',
    'ui.router',
    'ui.utils',
    'LocalStorageModule',
    'angular-loading-bar',
    'angular-momentjs',
    'angular-confirm',
    'FBAngular',
    'lazyModel',
    'toastr',
    'angularBootstrapNavTree',
    'oc.lazyLoad',
    'ui.select',
    'ui.tree',
    'textAngular',
    'colorpicker.module',
    'angularFileUpload',
    'ngImgCrop',
    'datatables',
    'datatables.bootstrap',
    'datatables.colreorder',
    'datatables.colvis',
    'datatables.tabletools',
    'datatables.scroller',
    'datatables.columnfilter',
    'ui.grid',
    'ui.grid.resizeColumns',
    'ui.grid.edit',
    'ui.grid.moveColumns',
    'ngTable',
    'smart-table',
    'angular-flot',
    'angular-rickshaw',
    'easypiechart',
    // 'uiGmapgoogle-maps',
    'ui.calendar',
    'ngTagsInput',
    'pascalprecht.translate',
    'ngMaterial',
    'localytics.directives',
    'leaflet-directive',
    'wu.masonry',
    'ipsum',
    'angular-intro',
    'dragularModule'
])

.run(['$rootScope', '$state', '$stateParams', '$location', function($rootScope, $state, $stateParams, $location) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;

    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {

        if (toState.hasOwnProperty('data') && toState.data.redirectTo) {
            event.preventDefault();

            $state.go(toState.data.redirectTo, toParams, {location: 'replace'});
        }
    });

    $rootScope.$on('$stateChangeSuccess', function(event, toState) {

        event.targetScope.$watch('$viewContentLoaded', function() {

            angular.element('html, body, #content').animate({ scrollTop: 0 }, 200);

            setTimeout(function() {
                angular.element('#wrap').css('visibility', 'visible');

                if (!angular.element('.dropdown').hasClass('open')) {
                    angular.element('.dropdown').find('>ul').slideUp();
                }
            }, 200);
        });

        if (toState.hasOwnProperty('data')) {
            $rootScope.containerClass = toState.data.containerClass;
        }
    });
}])

/**
 * General APP API constant
*/
.constant('apiConfig', {
    api: apiConfig.api.url,
    env: env,
    appConfig: apiConfig,
    userModel: 'EvusUser'
})

/**
 * General APP constant
 */
.constant('config', {
    modals: {
        styles: {
            default: 'splash splash-2 splash-ef-15',
            fullscreen: 'splash-1 splash-ef-1'
        }
    },
})

/**
 * Configuration for LoopBack API integration
*/
.config(function (LoopBackResourceProvider) {
    LoopBackResourceProvider.setAuthHeader('X-Access-Token');
    LoopBackResourceProvider.setUrlBase(apiConfig.api.url);
})

/**
 * Configure the UI select menu theme
*/
.config(['uiSelectConfig', function(uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
}])

/**
 * Configure localStorage. This is used by the
 * Auth.js and Storage.js services
*/
.config(['$localStorageProvider',
    function ($localStorageProvider) {
        $localStorageProvider.setKeyPrefix('EvusBoilerplate-');
    }
])

/**
 * Configure translate provider for supporting multiple languages
*/
.config(['$translateProvider', function($translateProvider) {
    $translateProvider.useStaticFilesLoader({
        prefix: 'languages/',
        suffix: '.json'
    });
    $translateProvider.useLocalStorage();
    $translateProvider.preferredLanguage('en');
    $translateProvider.useSanitizeValueStrategy(null);
}])

/**
 * App routing
*/
.config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider) {

    /**
     * Redirect root page request to login
    */
    $urlRouterProvider.when('', '/auth/login');

    /**
     * Bad URI's redirect to 404 page.
    */
    $urlRouterProvider.otherwise('/error/404');

    /**
     * Creating new namespaced pages?
     * http://patorjk.com/software/taag/#p=display&f=Blocks&t=layouts
    */
    $stateProvider
        // ==============================================================================================================================================
        // .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.
        // | .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
        // | |   _____      | || |      __      | || |  ____  ____  | || |     ____     | || | _____  _____ | || |  _________   | || |    _______   | |
        // | |  |_   _|     | || |     /  \     | || | |_  _||_  _| | || |   .'    `.   | || ||_   _||_   _|| || | |  _   _  |  | || |   /  ___  |  | |
        // | |    | |       | || |    / /\ \    | || |   \ \  / /   | || |  /  .--.  \  | || |  | |    | |  | || | |_/ | | \_|  | || |  |  (__ \_|  | |
        // | |    | |   _   | || |   / ____ \   | || |    \ \/ /    | || |  | |    | |  | || |  | '    ' |  | || |     | |      | || |   '.___`-.   | |
        // | |   _| |__/ |  | || | _/ /    \ \_ | || |    _|  |_    | || |  \  `--'  /  | || |   \ `--' /   | || |    _| |_     | || |  |`\____) |  | |
        // | |  |________|  | || ||____|  |____|| || |   |______|   | || |   `.____.'   | || |    `.__.'    | || |   |_____|    | || |  |_______.'  | |
        // | |              | || |              | || |              | || |              | || |              | || |              | || |              | |
        // | '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
        // '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'
        // ==============================================================================================================================================
        .state('admin', {
            abstract: true,
            url: '/admin',
            templateUrl: adminDir + 'admin.html'
        })
        .state('app', {
            abstract: true,
            url: '/app',
            templateUrl: appDir + 'app.html'
        })
        .state('auth', {
            abstract: true,
            url: '/auth',
            template: '<div ui-view></div>'
        })
        .state('error', {
            abstract: true,
            url: '/error',
            template: '<div ui-view></div>'
        })
        // =========================================================================================================
        // .----------------.  .----------------.  .----------------.  .----------------.
        // | .--------------. || .--------------. || .--------------. || .--------------. |
        // | |      __      | || | _____  _____ | || |  _________   | || |  ____  ____  | |
        // | |     /  \     | || ||_   _||_   _|| || | |  _   _  |  | || | |_   ||   _| | |
        // | |    / /\ \    | || |  | |    | |  | || | |_/ | | \_|  | || |   | |__| |   | |
        // | |   / ____ \   | || |  | '    ' |  | || |     | |      | || |   |  __  |   | |
        // | | _/ /    \ \_ | || |   \ `--' /   | || |    _| |_     | || |  _| |  | |_  | |
        // | ||____|  |____|| || |    `.__.'    | || |   |_____|    | || | |____||____| | |
        // | |              | || |              | || |              | || |              | |
        // | '--------------' || '--------------' || '--------------' || '--------------' |
        // '----------------'  '----------------'  '----------------'  '----------------'
        // =========================================================================================================
        .state('auth.home', {
            url: '/',
            templateUrl: authDir + 'login.html',
            controller: 'AuthLoginCtrl',
            data: {
                authenticate: false,
                loggedInRedirect: true,
            },
        })
        .state('auth.login', {
            url: '/login',
            templateUrl: authDir + 'login.html',
            controller: 'AuthLoginCtrl',
            data: {
                authenticate: false,
                loggedInRedirect: true,
            },
        })
        .state('auth.forgotpassword', {
            url: '/forgot',
            templateUrl: authDir + 'forgot.html',
            controller: 'AuthForgotCtrl',
            data: {
                authenticate: false,
                loggedInRedirect: true,
            },
        })
        .state('auth.logout', {
            url: '/logout',
            controller: 'AuthLogoutCtrl',
            data: {
                authenticate: false,
            },
        })
        .state('auth.signup', {
            url: '/signup',
            controller: 'AuthSignupCtrl',
            templateUrl: authDir + 'signup.html',
            data: {
                authenticate: false,
                loggedInRedirect: true,
            }
        })
        .state('auth.locked', {
            url: '/locked',
            controller: 'AuthLockedCtrl',
            templateUrl: authDir + 'locked.html',
            data: {
                authenticate: false,
                loggedInRedirect: true,
            }
        })
        // ==========================================================================================================================
        // .----------------.  .----------------.  .----------------.  .----------------.  .----------------.  .----------------.
        // | .--------------. || .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
        // | |  _________   | || |  _______     | || |  _______     | || |     ____     | || |  _______     | || |    _______   | |
        // | | |_   ___  |  | || | |_   __ \    | || | |_   __ \    | || |   .'    `.   | || | |_   __ \    | || |   /  ___  |  | |
        // | |   | |_  \_|  | || |   | |__) |   | || |   | |__) |   | || |  /  .--.  \  | || |   | |__) |   | || |  |  (__ \_|  | |
        // | |   |  _|  _   | || |   |  __ /    | || |   |  __ /    | || |  | |    | |  | || |   |  __ /    | || |   '.___`-.   | |
        // | |  _| |___/ |  | || |  _| |  \ \_  | || |  _| |  \ \_  | || |  \  `--'  /  | || |  _| |  \ \_  | || |  |`\____) |  | |
        // | | |_________|  | || | |____| |___| | || | |____| |___| | || |   `.____.'   | || | |____| |___| | || |  |_______.'  | |
        // | |              | || |              | || |              | || |              | || |              | || |              | |
        // | '--------------' || '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
        // '----------------'  '----------------'  '----------------'  '----------------'  '----------------'  '----------------'
        // ==========================================================================================================================
        .state('error.404', {
            url: '/404',
            templateUrl: errorDir + '404.html',
            data: {
                authenticate: false,
            }
        })
        .state('error.500', {
            url: '/500',
            templateUrl: errorDir + '500.html',
            data: {
                authenticate: false,
            }
        })
        .state('error.offline', {
            url: '/offline',
            templateUrl: errorDir + 'offline.html',
            data: {
                authenticate: false,
            }
        })
        // =========================================================================================================
        // .----------------.  .----------------.  .----------------.
        // | .--------------. || .--------------. || .--------------. |
        // | |      __      | || |   ______     | || |   ______     | |
        // | |     /  \     | || |  |_   __ \   | || |  |_   __ \   | |
        // | |    / /\ \    | || |    | |__) |  | || |    | |__) |  | |
        // | |   / ____ \   | || |    |  ___/   | || |    |  ___/   | |
        // | | _/ /    \ \_ | || |   _| |_      | || |   _| |_      | |
        // | ||____|  |____|| || |  |_____|     | || |  |_____|     | |
        // | |              | || |              | || |              | |
        // | '--------------' || '--------------' || '--------------' |
        // '----------------'  '----------------'  '----------------'
        // =========================================================================================================
        .state('app.legalPrivacy', {
            url: '/legal/privacy-policy',
            templateUrl: appDir + 'privacyPolicy/index/index.html',
            controller: 'AppPrivacyPolicyCtrl',
            data: {
                authenticate: false,
                containerClass: 'appPrivacyPolicy'
            }
        })
        .state('app.legalTerms', {
            url: '/legal/terms-and-conditions',
            templateUrl: appDir + 'terms/index/index.html',
            controller: 'AppTermsCtrl',
            data: {
                authenticate: false,
                containerClass: 'appPrivacyPolicy'
            }
        })
        // =========================================================================================================
        // .----------------.  .----------------.  .----------------.  .----------------.  .-----------------.
        // | .--------------. || .--------------. || .--------------. || .--------------. || .--------------. |
        // | |      __      | || |  ________    | || | ____    ____ | || |     _____    | || | ____  _____  | |
        // | |     /  \     | || | |_   ___ `.  | || ||_   \  /   _|| || |    |_   _|   | || ||_   \|_   _| | |
        // | |    / /\ \    | || |   | |   `. \ | || |  |   \/   |  | || |      | |     | || |  |   \ | |   | |
        // | |   / ____ \   | || |   | |    | | | || |  | |\  /| |  | || |      | |     | || |  | |\ \| |   | |
        // | | _/ /    \ \_ | || |  _| |___.' / | || | _| |_\/_| |_ | || |     _| |_    | || | _| |_\   |_  | |
        // | ||____|  |____|| || | |________.'  | || ||_____||_____|| || |    |_____|   | || ||_____|\____| | |
        // | |              | || |              | || |              | || |              | || |              | |
        // | '--------------' || '--------------' || '--------------' || '--------------' || '--------------' |
        // '----------------'  '----------------'  '----------------'  '----------------'  '----------------'
        // =========================================================================================================
        .state('admin.dashboard', {
            url: '/dashboard',
            resolve: {
                plugins: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'scripts/vendor/datatables/datatables.bootstrap.min.css'
                    ]);
                }]
            },
            data: {
                authenticate: true,
                containerClass: 'adminDashboard'
            },
            views: {
                '@admin': {
                    templateUrl: adminDir + 'dashboard/dashboard.html',
                    controller: 'AdminDashboardCtrl'
                }
            }
        })
        .state('admin.userProfile', {
            url: '/user/:id',
            resolve: {
                plugins: ['$ocLazyLoad', function($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        'scripts/vendor/datatables/datatables.bootstrap.min.css'
                    ]);
                }]
            },
            data: {
                authenticate: true,
                containerClass: 'adminUserProfile'
            },
            views: {
                '@admin': {
                    templateUrl: adminDir + 'users/profile/index.html',
                    controller: 'AdminUserProfileCtrl'
                }
            }
        });
    // END $states

    /**
     * @description
     * HTTP interceptor to catch authentication errors from request and
     * redirects the user to the login page. Keeps the last accessed route
     * to redirect the user after login to the page that caused the 401
     *
     * @TODO Need to write logic for the interceptor to know if the 401 came
     * from an API call and if it's eligible for redirection or if it is
     * even a valid route.
    */
    $httpProvider.defaults.headers.common.Author = 'Jared L Cowan';

    $httpProvider.interceptors.push(function ($q, $location, LoopBackAuth, $injector) {
        return {
            request: function (config) {
                return config;
            },
            requestError: function (rejection) {
                return $q.reject(rejection);
            },
            response: function (response) {
                return response;
            },
            responseError: function (rejection) {
                /**
                 * Don't try to catch error if logging in
                */
                if (rejection.status === 401 && rejection.data.error.code !== 'LOGIN_FAILED') {
                    // Clear all notifications
                    $injector.get('toastr').clear();

                    // Now clearing the loopback values from client browser for safe logout
                    LoopBackAuth.clearUser();
                    LoopBackAuth.clearStorage();

                    // Only set the location after first error
                    // Reset in login controller
                    if (!angular.isDefined($location.nextAfterLogin)) {
                        $location.nextAfterLogin = $location.path();
                    }

                    $location.path('/auth/login');

                    return $q.reject(rejection);
                } else if (rejection.status === -1) {
                    /**
                     * @description This is to redirect connection refused
                    */

                    $location.path('/auth/login');

                    return $q.reject(rejection);
                }

                return $q.reject(rejection);
            }
        };
    });
}]);
