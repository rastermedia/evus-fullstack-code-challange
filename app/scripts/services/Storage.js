'use strict';

app.service('Storage', function (
        $cookies,
        $localStorage,
        $window,
        LoopBackAuth,
        $log
    ) {
    /**
     * @description Check for undefined values and return null
    */
    this.nullify = function (value) {
        if (typeof value === 'undefined') {
            return null;
        }

        return value;
    };

    /**
     * @description Retrieve value based on key name
    */
    this.get = function (key) {
        return this.nullify($localStorage[key]);
    };

    /**
     * @description Set a key value pair
    */
    this.set = function (key, value) {
        $localStorage[key] = value;
        return value;
    };

    /**
     * @description Set or retrieve access token
    */
    this.token = function (accessToken) {
        if (this.nullify(accessToken) !== null) {
            $cookies.put('token', accessToken);
        }

        const token = this.nullify($cookies.get('token'));

        if (token === null) {
            $log.info('storage.token()', 'access token returned as null!');
        }

        return token;
    };

    /**
     * @description Destroy all cookies
    */
    this.destroy = function () {
        angular.forEach($cookies.getAll(), function (value, key) {
            $cookies.remove(key);
        });

        delete $localStorage.user;

        LoopBackAuth.clearUser();
        LoopBackAuth.clearStorage();

        $window.localStorage.clear();
    };
});
