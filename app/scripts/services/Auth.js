'use strict';

app.service('Auth', function (
        $rootScope,
        Storage,
        $q,
        apiConfig,
        $injector,
        localStorageService,
        $log,
        $cookies,
        $timeout
) {
    /**
     * @description To make this boilerplate as easy as possible, we set the UserModel
     * from the loopback API in app.js in the apiConfig app constant.
     *
     * Only use this method of getting the user model in these basic login controllers.
    */
    var USERMODEL = $injector.get(apiConfig.userModel);

    /**
     * @description Capitalizes a word/string
     *
     * @param  {string} word
     * @return {string} Word
     */
    this.capitalize = function (word) {
        return word.substring(0, 1).toUpperCase() + word.substring(1).toLowerCase();
    };

    /**
     * @description Return access token, userId, ttl, access token created timestamp
     *
     * @param  {object} credentials
     * @return {promise}  string|null
     */
    this.authorize = function (credentials) {
        return $q(function (resolve, reject) {
            USERMODEL.login({}, credentials, function (loginPromiseRes) {
                resolve(loginPromiseRes);
            }, function (loginPromiseResErr) {
                reject(loginPromiseResErr);
            });
        });
    };

    /**
     * @description Get current access token or set it
     *
     * @param  accessToken {string}
     * @return {string|null}
     */
    this.token = function (accessToken) {
        return Storage.token(accessToken);
    };

    /**
     * @description Login procedure
     * set cookies, call api endpoints and anything else needed to make app happy
     *
     * @param  {object} credentials
     * @return {promise} string|null
     */
     this.login = function (credentials) {
        return $q(function (resolve, reject) {
            USERMODEL.login({}, credentials, function (loginPromiseRes) {
                if (loginPromiseRes.hasOwnProperty('error')) {
                    reject('LOGIN_FAILED');
                } else {

                    Storage.set('user', loginPromiseRes);
                    resolve(loginPromiseRes);
                }
            }, function (loginPromiseResErr) {
                reject(loginPromiseResErr);
            });
        });
    };

    /**
     * @description Returns all user related data
     *
     * @return {object} user data
     */
    this.user = (function() {
        if (Storage.get('user')) {
            return Storage.get('user').user;
        } else {
            return null;
        };
    })();

    /**
     * @description Destroy auth session
     *
     * @return {void}
     */
    this.destroy = function () {
        Storage.destroy();
    };
});
