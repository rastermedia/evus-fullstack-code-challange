'use strict';

app.controller('AdminTopNavCtrl', function (
    $rootScope,
    $scope,
    $state,
    $stateParams,
    $translate,
    toastr,
    $interval,
    $q,
    $log,
    Auth,
    Storage,
    $localStorage,
    LoopBackAuth
) {
    // =========================================================================================================
    // Defaults and definitions
    // =========================================================================================================
    $scope.resource = {
        userName: Storage.get('user').user.firstName + ' ' + Storage.get('user').user.lastName,
        notifications: null,
        supportedLangs: [
            {
                name: 'English',
                langKey: 'en',
                flag: 'images/flags/United-States-of-America.png',
                enabled: true
            },
            {
                name: 'Czech',
                langKey: 'cz',
                flag: 'images/flags/Czech-Republic.png',
                enabled: false
            },
            {
                name: 'Germany',
                langKey: 'de',
                flag: 'images/flags/Germany.png',
                enabled: false
            }
        ]
    };

    // =========================================================================================================
    // Promises
    // =========================================================================================================

    // =========================================================================================================
    // Init Function
    // =========================================================================================================
    var init = function () {
        $scope.getNotifications();
    };

    // =========================================================================================================
    // Functions
    // =========================================================================================================
    $scope.getNotifications = function () {
        $scope.resource.notifications = [
            {
                "title": "My first notification",
                "isRead": false,
                "createdDate": new Date(),
                "id": 1
            },
            {
                "title": "My Second notification",
                "isRead": false,
                "createdDate": new Date(),
                "id": 2
            },
            {
                "title": "My third notification",
                "isRead": false,
                "createdDate": new Date(),
                "id": 3
            }
        ];
    };

    /**
     * @description Method to change language in site where | translate is used
    */
    $scope.changeLanguage = function (langKey) {
        $translate.use(langKey);
        $scope.currentLanguage = langKey;
    };

    /**
     * Default the current language
    */
    $scope.currentLanguage = $translate.proposedLanguage() || $translate.use();

    /**
     * @description Method to logOut
     *
     * @returns {$state}
    */
    $scope.logOut = function() {
        Auth.destroy();
        $state.go('auth.login');
    };

    /**
     * @description Method to lockOut
     *
     * @returns {$state}
     */
    $scope.lockOut = function() {
        LoopBackAuth.clearUser();
        LoopBackAuth.clearStorage();
        delete $localStorage.user.id;
        $state.go('auth.locked');
    };

    $scope.markAsRead = function (event, id, index) {
        $scope.resource.notifications.splice(index, 1);
    };

    // =========================================================================================================
    // Initialize app
    // =========================================================================================================
    init();
});
