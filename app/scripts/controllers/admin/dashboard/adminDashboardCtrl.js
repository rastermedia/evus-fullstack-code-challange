'use strict';

app.controller('AdminDashboardCtrl', function (
    $rootScope,
    $scope,
    $state,
    EvusUser,
    $q,
    $log,
    toastr,
    DTOptionsBuilder,
    DTColumnDefBuilder,
    Auth
) {
    // =========================================================================================================
    // Defaults and Definitions
    // =========================================================================================================
    $scope.resource = {
        page: {
            name: 'Dashboard'
        },
        tables: {
            users: {
                name: 'Users',
                data: null,
                options: DTOptionsBuilder.newOptions().withPaginationType('full_numbers')
                          .withBootstrap()
                          .withOption('responsive', true)
                          .withOption('order', [[0, 'asc']])
                          .withOption('stateSave', true),
                columns: [
                    DTColumnDefBuilder.newColumnDef(0),
                    DTColumnDefBuilder.newColumnDef(1),
                    DTColumnDefBuilder.newColumnDef(2).notSortable(),
                ],
            },
        }
    };

    // =========================================================================================================
    // Promises
    // =========================================================================================================
    /**
     * @description
     *
     * @returns {promise}
    */
    var getUsers = function () {
        var defer = $q.defer();

        EvusUser.find(function (getUsersRes) {
            defer.resolve(getUsersRes);
        }, function (getUsersResErr) {
            defer.reject(getUsersResErr);
        });

        return defer.promise;
    };

    // =========================================================================================================
    // Initialize controller
    // =========================================================================================================
    var _init = function () {
        $scope.getUsers();
    };

    // =========================================================================================================
    // Functions
    // =========================================================================================================
    $scope.getUsers = function () {
        getUsers().then(function (getUsersPromiseRes) {
            $scope.resource.tables.users.data = getUsersPromiseRes;
        }).catch(function (getUsersPromiseResErr) {
            $log.error(getUsersPromiseResErr);
            toastr.error("Error fetching data for users", "Error");
        });
    };

    _init();
});
