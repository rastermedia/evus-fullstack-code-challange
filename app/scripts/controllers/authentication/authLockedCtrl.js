'use strict';

app.controller('AuthLockedCtrl', function (
    $rootScope,
    $scope,
    $state,
    $stateParams,
    $q,
    $log,
    toastr,
    LoopBackAuth,
    Storage,
    Auth
) {
    // =========================================================================================================
    // Defaults and Definitions
    // =========================================================================================================
    $scope.resource = {
        fullName: null, // set in checkEligibilityForLockedLogin()
        user: {},
    };

    var checkEligibilityForLockedLogin = function() {
        try {
            Storage.get('user').user.email;
            $scope.resource.fullName = Storage.get('user').user.firstName + ' ' + Storage.get('user').user.lastName;
        } catch (err) {
            toastr.error("Please login again");
            $state.go('auth.login');
        }
    };

    checkEligibilityForLockedLogin();

    /**
     * @description Method handling form login
     *
     * @returns {$state}
     */
    $scope.lockedLogin = function () {
        Auth.login({
            email: Storage.get('user').user.email,
            password: $scope.resource.user.password,
        }).then(function (userLoginRes) {

            if (userLoginRes.hasOwnProperty('error')) {
                checkEligibilityForLockedLogin();

                toastr.error("Password incorrect", "Login Error");
            } else {
                toastr.success("Welcome back!");
                $state.go('admin.dashboard');
            }
        }).catch(function (userLoginResErr) {
            toastr.error("Password incorrect", "Login Error");
            $log.error(userLoginResErr);
        });
    };

    /**
     * @description Function to clear all data if the wrong user is shown on quick login screen
     *
     * @returns {$state}
    */
    $scope.notYou = function () {
        Auth.destroy();

        // Go to login screen
        $state.go('auth.login');
    };
});
