'use strict';

app.controller('AuthSignupCtrl', function (
    $scope,
    $state,
    $q,
    $log,
    toastr,
    EvusUser,
    Auth,
    Storage
) {
    // =========================================================================================================
    // Defaults and definitions
    // =========================================================================================================
    $scope.resource = {
        user: {}
    };

    // =========================================================================================================
    // Promises
    // =========================================================================================================
    /**
     * @description Creates a new EvusUser instance
     *
     * @param {object} userObj - EvusUser object
     * @param {string} userObj.email - Email address
     * @param {string} userObj.password - Password
     *
     * @returns {promise}
    */
    var createUser = function (userObj) {
        var defer = $q.defer();

        EvusUser.create(userObj, function (createUserRes) {
            defer.resolve(createUserRes);
        }, function (createUserResErr) {
            defer.reject(createUserResErr);
        });

        return defer.promise;
    };

    // =========================================================================================================
    // Functions
    // =========================================================================================================
    /**
     * @description Form submit for creating user
     *
     * @param {boolean} valid - Boolean if for is valid
     *
     * @returns {$state|toastr}
    */
    $scope.submitUserForm = function (valid) {
        if (!valid) {
            return toastr.error("Error: Incomplete form", "Please complete all form fields.");
        }

        createUser($scope.resource.user).then(function (createUserPromiseRes) {
            toastr.success("Please login with the credentials you just created.", "Welcome!");
            $state.go('auth.login');
        }).catch(function (createUserPromiseRes) {
            toastr.error(createUserPromiseRes, "Error");
            $log.error(createUserPromiseRes);
        });
    };
});
