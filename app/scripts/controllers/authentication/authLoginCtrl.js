'use strict';

app.controller('AuthLoginCtrl', function (
    $rootScope,
    $scope,
    $state,
    $stateParams,
    $location,
    $log,
    $timeout,
    $window,
    Auth,
    toastr,
    LoopBackAuth
) {
    // =========================================================================================================
    // Defaults and Definitions
    // =========================================================================================================
    $scope.resource = {
        user: {},
    };

    /**
     * Clear any previous user data on page load
    */
    try {
        Auth.destroy();
    } catch (err) {};

    /**
     * Check to see if this was a redirection from a 401 and show message
    */
    if (angular.isDefined($location.nextAfterLogin)) {
        $timeout(function () {
            // We delay the alert just a little to allow the
            // $http interceptor to clear the current alerts
            // Otherwise this would also be cleared on page load. See app.js
            toastr.error("Please login to continue", "Authorization required");
        }, 300);
    }

    /**
     * @description Method handling form login
     *
     * @param {isValid} isValid - Boolean
     *
     * @returns {$state|$location|toastr}
    */
    $scope.login = function (isValid) {
        if (!isValid) {
            return toastr.error("Please complete all form fields", "Error: Incomplete form");
        }

        Auth.login($scope.resource.user).then(function (userLoginRes) {

            if (angular.isDefined($location.nextAfterLogin)) {
                $location.path($location.nextAfterLogin);
                $location.nextAfterLogin = undefined;
            } else {
                toastr.success("Welcome back!");
                $state.go('admin.dashboard', {reload: true});
            }

        }).catch(function (userLoginResErr) {
            toastr.error("Email or password incorrect", "Login Error");
            $log.error(userLoginResErr);
        });
    };
});
