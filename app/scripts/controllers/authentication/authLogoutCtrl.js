'use strict';

app.controller('AuthLogoutCtrl', function (
    $rootScope,
    $scope,
    $state,
    $log,
    LoopBackAuth,
    Auth
) {
   /**
    * @description When this page is accessed, run logout method and redirect
    *
    * @returns {$state}
   */
   Auth.destroy();
   $state.go('auth.login');
});
