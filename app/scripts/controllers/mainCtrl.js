'use strict';

app.controller('MainCtrl', function (
    $scope,
    $http
) {
    // =========================================================================================================
    // Defaults and definitions
    // =========================================================================================================
    $scope.main = {
        title: 'EvusBoilerplate',
        settings: {
            navbarHeaderColor: 'scheme-black',
            sidebarColor: 'scheme-black',
            brandingColor: 'scheme-black',
            activeColor: 'cyan-scheme-color',
            headerFixed: true,
            asideFixed: true
        }
    };

    // =========================================================================================================
    // Promises
    // =========================================================================================================

    // =========================================================================================================
    // Functions
    // =========================================================================================================
    $scope.ajaxFaker = function () {
        $scope.data=[];
        var url = 'http://www.filltext.com/?rows=10&fname={firstName}&lname={lastName}&delay=5&callback=JSON_CALLBACK';

        $http.jsonp(url).success(function(data){
            $scope.data=data;
            angular.element('.tile.refreshing').removeClass('refreshing');
        });
    };
});
