# Evus code challenge

## Getting Started
1. Clone this boilerplate and name your project

``$ git clone git@bitbucket.org:rastermedia/evus-fullstack-code-challange.git YOUR_PROJECT_NAME``

2. Make sure to install ``$ npm -g strongloop``
3. Install npm: ``$ npm install``
4. Install bower: ``$ npm install -g bower``
5. ``$ npm run init``
6. Create the API file ``$ npm run api``
---

## How to run the project
1. In root, create .env file
```
# Example .env file
ENV=development
```

3. Run the project with ``$ npm run start`` (You don't use Grunt)
4. Run the api with ``$ npm run start-api``

> Every time you make changes to the API files, you need to rebuild the lb-services file
> to do this, run ``$ npm run api``

[1]: https://bitbucket.org/rastermedia/admin-angular-boilerplate/src/c4e12ef7bff28ef398f790d660da13a5897725a9/app/scripts/app.js?at=master&fileviewer=file-view-default#app.js-161
